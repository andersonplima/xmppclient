﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VsoftXMPPClient
{
    public partial class UserForm : Form
    {
        private string server;
        private string username;
        private string password;

        public UserForm()
        {
            InitializeComponent();
        }

        public UserForm(string server, string username, string password) : this()
        {
            this.server = server;
            this.username = username;
            this.password = password;
        }

        public string Server
        {
            get
            {
                return server;
            }
        }

        public string Username
        {
            get
            {
                return username;
            }
        }

        public string Password
        {
            get
            {
                return password;
            }
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {

        }

        private void ServerTextBox_TextChanged(object sender, EventArgs e)
        {
            server = ServerTextBox.Text;
        }

        private void UsernameTextBox_TextChanged(object sender, EventArgs e)
        {
            username = UsernameTextBox.Text;
        }

        private void PasswordTextBox_TextChanged(object sender, EventArgs e)
        {
            password = PasswordTextBox.Text;
        }

        private void UserForm_Load(object sender, EventArgs e)
        {
            ServerTextBox.Text = Properties.Settings.Default.Server;
            UsernameTextBox.Text = Properties.Settings.Default.Username;
            PasswordTextBox.Text = Properties.Settings.Default.Password;
        }
    }
}
