﻿using S22.Xmpp;
using S22.Xmpp.Client;
using S22.Xmpp.Im;
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using System.Collections.Concurrent;
using System.Media;

namespace VsoftXMPPClient
{
    public partial class RosterForm : Form
    {
        XmppClient client;
        ConcurrentDictionary<Jid, SingleContactMessageWindow> windows;
        SoundPlayer messageSound;

        public RosterForm()
        {
            InitializeComponent();
            windows = new ConcurrentDictionary<Jid, SingleContactMessageWindow>();
            messageSound = new SoundPlayer("Message.wav");
            messageSound.Load();
        }

        private void Connect()
        {
            using (var f = new UserForm())
                if (f.ShowDialog(this) == DialogResult.OK)
                {
                    var username = f.Username.Contains("@") ? f.Username : f.Username + "@" + f.Server;
                    client = new XmppClient(f.Server, username, f.Password,
                        validate: (a, b, c, d) => true);
                    client.Error += Client_Error;
                    try
                    {
                        client.Connect("vsoft-client");
                        if (client.Connected)
                        {
                            tsStatus.Image = tsOnline.Image;

                            Properties.Settings.Default.Server = f.Server;
                            Properties.Settings.Default.Username = f.Username;
                            Properties.Settings.Default.Password = f.Password;

                            client.StatusChanged += Client_StatusChanged;
                            client.Message += Client_Message;
                            client.ChatStateChanged += Client_ChatStateChanged;

                            client.GetRoster();
                        }
                    }
                    catch
                    {
                        CloseClient();
                        MessageBox.Show("Erro ao autenticar!", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
        }

        private void RosterForm_Shown(object sender, EventArgs e)
        {
            if (client == null)
            {
                Connect();
                client.SetStatus(Availability.Online);
            }
        }

        private SingleContactMessageWindow CreateMessageWindow(Jid jid)
        {
            var window = new SingleContactMessageWindow(client, jid);
            windows[jid] = window;
            window.FormClosed += Window_FormClosed;
            return window;
        }

        private void Window_FormClosed(object sender, FormClosedEventArgs e)
        {
            var window = sender as SingleContactMessageWindow;
            windows.TryRemove(window.Contact, out window);
        }

        private void Client_ChatStateChanged(object sender, S22.Xmpp.Extensions.ChatStateChangedEventArgs e)
        {
            var jid = e.Jid;
            var chatState = e.ChatState;
            Invoke(new Action(() =>
            {
                SingleContactMessageWindow window;
                if (!windows.TryGetValue(jid, out window))
                    window = CreateMessageWindow(jid);

                window.AddChatState(chatState);
            }));
        }

        private void Client_Message(object sender, MessageEventArgs e)
        {
            var jid = e.Jid;
            var time = e.Message.Timestamp;
            var message = e.Message.Body;
            Invoke(new Action(() =>
            {
                var new_window = false;
                SingleContactMessageWindow window;
                if (!windows.TryGetValue(jid, out window))
                {
                    window = CreateMessageWindow(jid);
                    new_window = true;
                }

                window.AddReceivedMessage(time, message);                
                PlaySound();                
                if (new_window && (ActiveForm is RosterForm || ActiveForm == null))
                {
                    window.Show(this);
                    window.Activate();
                }           
                else
                {
                    var item = rosterListView.Items.Cast<ListViewItem>().Where(i => ((Jid)i.Tag).Node == e.Jid.Node).FirstOrDefault();
                    item.Font = new System.Drawing.Font(item.Font, System.Drawing.FontStyle.Bold);
                }     
            }));
        }

        private void PlaySound()
        {
            messageSound.Play();
        }

        private void Client_StatusChanged(object sender, S22.Xmpp.Im.StatusEventArgs e)
        {
            var jid = e.Jid;
            var availability = e.Status.Availability;

            rosterListView.Invoke(new Action(() =>
            {
                if (availability != Availability.Offline)
                {
                    var item = rosterListView.Items.Cast<ListViewItem>().Where(i => ((Jid)i.Tag).Node == e.Jid.Node).FirstOrDefault();
                    if (item == null)
                    {
                        item = new ListViewItem();
                        item.Text = jid.Node;

                        rosterListView.Items.Add(item);
                    }

                    item.Tag = jid;
                    item.ImageIndex = (int)availability;
                }
                else
                {
                    var item = rosterListView.Items.Cast<ListViewItem>().Where(i => ((Jid)i.Tag).Node == e.Jid.Node).FirstOrDefault();
                    if (item != null)
                        item.Remove();

                    SingleContactMessageWindow removed_window;
                    if (windows.TryRemove(jid, out removed_window))
                        removed_window.Close();
                }
            }));

        }

        private void Client_Error(object sender, S22.Xmpp.Im.ErrorEventArgs e)
        {

        }

        private void rosterListView_ItemActivate(object sender, EventArgs e)
        {
            var activatedItem = rosterListView.SelectedItems[0];
            activatedItem.Font = new System.Drawing.Font(activatedItem.Font, System.Drawing.FontStyle.Regular);

            SingleContactMessageWindow window;
            var jid = (Jid)activatedItem.Tag;
            if (!windows.TryGetValue(jid, out window))
            {
                window = new SingleContactMessageWindow(client, jid);
                window.FormClosing += Window_FormClosing;
                windows[jid] = window;
            }

            window.Show();
            window.Activate();
        }

        private void Window_FormClosing(object sender, FormClosingEventArgs e)
        {
            var window = sender as SingleContactMessageWindow;
            windows.TryRemove(window.Contact, out window);
        }

        private void CloseClient()
        {
            if (client != null)
            {
                if (client.Connected)
                    client.Dispose();

                client = null;
            }
        }

        private void RosterForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            CloseClient();
        }

        private void CleanAll()
        {
            foreach (var jid in windows.Keys)
            {
                var window = windows[jid];
                window.Close();
            }

            rosterListView.Items.Clear();
        }

        private void tsOffline_Click(object sender, EventArgs e)
        {
            if (client != null)
            {
                CloseClient();
                CleanAll();

                tsStatus.Image = tsOffline.Image;
            }
        }

        private void tsOnline_Click(object sender, EventArgs e)
        {
            if (client == null)
                Connect();

            client.SetStatus(Availability.Online);
            tsStatus.Image = tsOnline.Image;
        }

        private void tsAusente_Click(object sender, EventArgs e)
        {
            if (client == null)
                Connect();

            client.SetStatus(Availability.Away);
            tsStatus.Image = tsAusente.Image;
        }

        private void tsChat_Click(object sender, EventArgs e)
        {
            if (client == null)
                Connect();

            client.SetStatus(Availability.Chat);
            tsStatus.Image = tsChat.Image;
        }

        private void tsDND_Click(object sender, EventArgs e)
        {
            if (client == null)
                Connect();

            client.SetStatus(Availability.Dnd);
            tsStatus.Image = tsDND.Image;
        }

        private void notifyIcon1_Click(object sender, EventArgs e)
        {
            if (!Visible)
            {
                Show();
                ShowInTaskbar = true;
            }
            else
            {
                Hide();
                ShowInTaskbar = false;
            }
        }

        private void RosterForm_Resize(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Minimized)
            {
                Hide();
                ShowInTaskbar = false;
            }
        }
    }
}
