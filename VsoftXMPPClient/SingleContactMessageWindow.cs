﻿using S22.Xmpp;
using S22.Xmpp.Client;
using S22.Xmpp.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VsoftXMPPClient
{
    public partial class SingleContactMessageWindow : Form
    {
        private XmppClient client;
        private Jid contact;

        public Jid Contact
        {
            get
            {
                return contact;
            }
        }

        public SingleContactMessageWindow()
        {
            InitializeComponent();
        }

        public SingleContactMessageWindow(XmppClient client, Jid contact) : this()
        {
            this.client = client;
            this.contact = contact;
            this.Text = $"Conversa com {contact}...";
        }

        private void messageWindow_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Control)
            {
                if (e.KeyCode == Keys.Enter)
                {
                    e.SuppressKeyPress = true;
                    var message = messageWindow.Text;
                    message  = message.TrimEnd('\r', '\n');
                    var time = DateTime.Now;
                    this.client.SendMessage(Contact, message);
                    messageWindow.Clear();

                    UpdateConversationWindow(message, time);
                }
            }
        }

        public void AddReceivedMessage(DateTime time, string message)
        {
            message = string.Join(Environment.NewLine, message.Split('\n'));
            UpdateConversationWindow(message, time, false);
        }

        public void AddChatState(ChatState chatState)
        {
            switch (chatState)
            {
                case ChatState.Active:
                    chatStatusStrip.Text = "Chat ativo.";
                    break;
                case ChatState.Inactive:
                    chatStatusStrip.Text = "Chat inativo";
                    break;
                case ChatState.Gone:
                    chatStatusStrip.Text = "O contato fechou a janela";
                    break;
                case ChatState.Composing:
                    chatStatusStrip.Text = "O contato está escrevendo uma mensagem...";
                    break;
                case ChatState.Paused:
                    chatStatusStrip.Text = "O contato parou de escrever.";
                    break;
                default:
                    break;
            }            
        }

        private void UpdateConversationWindow(string message, DateTime time, bool outcoming = true)
        {
            var text = outcoming ?
                 $"{time: dd/MM/yyyy HH:mm} < {client.Jid}: {message}{Environment.NewLine}" :
                 $"{time: dd/MM/yyyy HH:mm} > {Contact}: {message}{Environment.NewLine}";

            conversationWindow.AppendText(text);
        }

        private void SingleContactMessageWindow_Shown(object sender, EventArgs e)
        {
            messageWindow.Focus();
        }
    }
}
