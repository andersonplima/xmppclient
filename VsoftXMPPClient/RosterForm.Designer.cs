﻿namespace VsoftXMPPClient
{
    partial class RosterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RosterForm));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tsStatus = new System.Windows.Forms.ToolStripDropDownButton();
            this.tsOffline = new System.Windows.Forms.ToolStripMenuItem();
            this.tsOnline = new System.Windows.Forms.ToolStripMenuItem();
            this.tsAusente = new System.Windows.Forms.ToolStripMenuItem();
            this.tsChat = new System.Windows.Forms.ToolStripMenuItem();
            this.tsDND = new System.Windows.Forms.ToolStripMenuItem();
            this.RosterImageList = new System.Windows.Forms.ImageList(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.rosterListView = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.statusStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsStatus});
            this.statusStrip1.Location = new System.Drawing.Point(0, 456);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 16, 0);
            this.statusStrip1.Size = new System.Drawing.Size(225, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tsStatus
            // 
            this.tsStatus.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsStatus.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsOffline,
            this.tsOnline,
            this.tsAusente,
            this.tsChat,
            this.tsDND});
            this.tsStatus.Image = ((System.Drawing.Image)(resources.GetObject("tsStatus.Image")));
            this.tsStatus.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsStatus.Name = "tsStatus";
            this.tsStatus.Size = new System.Drawing.Size(29, 20);
            this.tsStatus.Text = "toolStripDropDownButton1";
            // 
            // tsOffline
            // 
            this.tsOffline.Image = ((System.Drawing.Image)(resources.GetObject("tsOffline.Image")));
            this.tsOffline.Name = "tsOffline";
            this.tsOffline.Size = new System.Drawing.Size(144, 22);
            this.tsOffline.Text = "Offline";
            this.tsOffline.Click += new System.EventHandler(this.tsOffline_Click);
            // 
            // tsOnline
            // 
            this.tsOnline.Image = ((System.Drawing.Image)(resources.GetObject("tsOnline.Image")));
            this.tsOnline.Name = "tsOnline";
            this.tsOnline.Size = new System.Drawing.Size(144, 22);
            this.tsOnline.Text = "Online";
            this.tsOnline.Click += new System.EventHandler(this.tsOnline_Click);
            // 
            // tsAusente
            // 
            this.tsAusente.Image = ((System.Drawing.Image)(resources.GetObject("tsAusente.Image")));
            this.tsAusente.Name = "tsAusente";
            this.tsAusente.Size = new System.Drawing.Size(144, 22);
            this.tsAusente.Text = "Ausente";
            this.tsAusente.Click += new System.EventHandler(this.tsAusente_Click);
            // 
            // tsChat
            // 
            this.tsChat.Image = ((System.Drawing.Image)(resources.GetObject("tsChat.Image")));
            this.tsChat.Name = "tsChat";
            this.tsChat.Size = new System.Drawing.Size(144, 22);
            this.tsChat.Text = "Chat";
            this.tsChat.Click += new System.EventHandler(this.tsChat_Click);
            // 
            // tsDND
            // 
            this.tsDND.Image = ((System.Drawing.Image)(resources.GetObject("tsDND.Image")));
            this.tsDND.Name = "tsDND";
            this.tsDND.Size = new System.Drawing.Size(144, 22);
            this.tsDND.Text = "Não perturbe";
            this.tsDND.Click += new System.EventHandler(this.tsDND_Click);
            // 
            // RosterImageList
            // 
            this.RosterImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("RosterImageList.ImageStream")));
            this.RosterImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.RosterImageList.Images.SetKeyName(0, "1439866921_empathy-offline.ico");
            this.RosterImageList.Images.SetKeyName(1, "1439866978_user-online.ico");
            this.RosterImageList.Images.SetKeyName(2, "1439866988_empathy-away.ico");
            this.RosterImageList.Images.SetKeyName(3, "1439867119_im-message-new.ico");
            this.RosterImageList.Images.SetKeyName(4, "1439866948_tray-busy.ico");
            this.RosterImageList.Images.SetKeyName(5, "1439867029_empathy-extended-away.ico");
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.rosterListView);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(225, 456);
            this.panel1.TabIndex = 2;
            // 
            // rosterListView
            // 
            this.rosterListView.Activation = System.Windows.Forms.ItemActivation.OneClick;
            this.rosterListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this.rosterListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rosterListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.rosterListView.LargeImageList = this.RosterImageList;
            this.rosterListView.Location = new System.Drawing.Point(0, 0);
            this.rosterListView.MultiSelect = false;
            this.rosterListView.Name = "rosterListView";
            this.rosterListView.Size = new System.Drawing.Size(225, 456);
            this.rosterListView.SmallImageList = this.RosterImageList;
            this.rosterListView.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.rosterListView.TabIndex = 1;
            this.rosterListView.UseCompatibleStateImageBehavior = false;
            this.rosterListView.View = System.Windows.Forms.View.List;
            this.rosterListView.ItemActivate += new System.EventHandler(this.rosterListView_ItemActivate);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Width = 230;
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.notifyIcon1.BalloonTipText = "O XMPP Client continuará executando.";
            this.notifyIcon1.BalloonTipTitle = "Informação";
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.Click += new System.EventHandler(this.notifyIcon1_Click);
            // 
            // RosterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(225, 478);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.statusStrip1);
            this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "RosterForm";
            this.Text = "XMPP Client";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.RosterForm_FormClosing);
            this.Shown += new System.EventHandler(this.RosterForm_Shown);
            this.Resize += new System.EventHandler(this.RosterForm_Resize);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ImageList RosterImageList;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ListView rosterListView;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ToolStripDropDownButton tsStatus;
        private System.Windows.Forms.ToolStripMenuItem tsOffline;
        private System.Windows.Forms.ToolStripMenuItem tsOnline;
        private System.Windows.Forms.ToolStripMenuItem tsAusente;
        private System.Windows.Forms.ToolStripMenuItem tsChat;
        private System.Windows.Forms.ToolStripMenuItem tsDND;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
    }
}